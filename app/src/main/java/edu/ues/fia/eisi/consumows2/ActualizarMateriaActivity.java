package edu.ues.fia.eisi.consumows2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ActualizarMateriaActivity extends AppCompatActivity {
    private static final String Tag = "Mensajes";


    ControlBD db;

    static List<String> list;
    ListView listView;

    EditText fechaTxt;

    ArrayList<Materia> lista;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;


    private final String urlLocal = "http://192.168.1.8/filtrarMateria.php?";
    private final String urlHostingGratuito = "https://oo15004pdm115.000webhostapp.com/ws_filtrarMateria.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_materia);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        db = new ControlBD(this);
        lista = new ArrayList<Materia>();
        list = new ArrayList<String>();
        fechaTxt = (EditText) findViewById(R.id.editText_fecha);
        listView = (ListView) findViewById(R.id.listView1);

    }

    public void servicioPHP(View v) {
        String[] fecha = fechaTxt.getText().toString().split("/"); String url = "";
        switch(v.getId()) {
            case R.id.button1:
                url = urlLocal + "day=" + fecha[0] + "&month="+ fecha[1] + "&year=" + fecha[2];
                Log.i(Tag, url);
                break;
            case R.id.button2:
                url = urlHostingGratuito + "day=" + fecha[0] + "&month="+ fecha[1] + "&year=" + fecha[2];
                Log.i(Tag, url);
                break;
        }

        String json = ControladorServicio.obtenerRespuestaPeticion(url, this);

        try {
            List<Materia> ls = new ArrayList<Materia>();
            ls = ControladorServicio.obtenerLista(json, this);
            for (Materia lsp : ls){
                lista.add(lsp);
            }
            llenarTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardar(View v) {
        Toast.makeText(this, db.insertaArray(lista), Toast.LENGTH_LONG).show();
        lista.clear();
        llenarTabla();
    }


    private void llenarTabla(){

        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (Materia a : lista){
                stringArrayList.add(a.getCodMateria()+" "+a.getNombreMateria());
            }
        }else {
            stringArrayList.add("Lista Vacia");
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

}
