package edu.ues.fia.eisi.consumows2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class IngresarNotaActivity extends AppCompatActivity {

    private final String urlLocal = "http://192.168.1.8/insertarNota.php?";
    private final String urlHostingGratuito = "https://oo15004pdm115.000webhostapp.com/ws_insertarNota.php?";

    TextView txt, txt2,txt3,txt4;
    Button btn4, btn5;
    EditText carnetTxt;
    EditText codMateriaTxt;
    EditText cicloTxt;
    EditText notaTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_nota);
        txt = findViewById(R.id.textView);
        txt2 = findViewById(R.id.textView2);
        txt3 = findViewById(R.id.textView3);
        txt4 = findViewById(R.id.textView4);
        txt.setText("Carnet: ");
        txt2.setText("Codigo Materia: ");
        txt3.setText("Ciclo: ");
        txt4.setText("Nota Final: ");


        btn4 = findViewById(R.id.button4);
        btn5 = findViewById(R.id.button5);
        btn4.setText("LOCAL PHP");
        btn5.setText("HOSTING PHP");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        carnetTxt = (EditText) findViewById(R.id.editText);
        codMateriaTxt = (EditText) findViewById(R.id.editText2);
        cicloTxt = (EditText) findViewById(R.id.editText3);
        notaTxt = (EditText) findViewById(R.id.editText4);


    }

    public void ingresarLocal(View v){
        String carnet = carnetTxt.getText().toString();
        String codMateria = codMateriaTxt.getText().toString();
        String ciclo = cicloTxt.getText().toString();
        String notaFinal = notaTxt.getText().toString();
        String url = urlLocal+ "carnet=" + carnet + "&codmateria=" + codMateria + "&ciclo=" + ciclo + "&notafinal=" + notaFinal;
        ControladorServicio.insertarNotaExterno(url, this);
    }
    public void ingresarHost(View v){
        String carnet = carnetTxt.getText().toString();
        String codMateria = codMateriaTxt.getText().toString();
        String ciclo = cicloTxt.getText().toString();
        String notaFinal = notaTxt.getText().toString();
        String url = urlHostingGratuito+ "carnet=" + carnet + "&codmateria=" + codMateria + "&ciclo=" + ciclo + "&notafinal=" + notaFinal;
        ControladorServicio.insertarNotaExterno(url, this);
    }
}
