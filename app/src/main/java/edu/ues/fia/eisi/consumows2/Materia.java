package edu.ues.fia.eisi.consumows2;

public class Materia {
    String CodMateria, NombreMateria, UnidadesVal;

    public Materia() {
    }

    public Materia(String codMateria, String nombreMateria, String unidadesVal) {
        CodMateria = codMateria;
        NombreMateria = nombreMateria;
        UnidadesVal = unidadesVal;
    }

    public String getCodMateria() {
        return CodMateria;
    }

    public void setCodMateria(String codMateria) {
        CodMateria = codMateria;
    }

    public String getNombreMateria() {
        return NombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        NombreMateria = nombreMateria;
    }

    public String getUnidadesVal() {
        return UnidadesVal;
    }

    public void setUnidadesVal(String unidadesVal) {
        UnidadesVal = unidadesVal;
    }
}
