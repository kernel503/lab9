package edu.ues.fia.eisi.consumows2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class PromedioAlumnoActivity extends AppCompatActivity {

    private final String urlLocal = "http://192.168.1.8/promedioAlumno.php?";
    private final String urlHostingGratuito = "https://oo15004pdm115.000webhostapp.com/ws_promedioAlumno.php?";
    EditText carnetTxt;
    TextView notaPromedioTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promedio_alumno);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        carnetTxt = (EditText) findViewById(R.id.editText_alumnoCarnet);
        notaPromedioTxt = (TextView) findViewById(R.id.textView_alumnoNota);
    }

    public void consultarPromedioLocal(View v) {
        String carnet = carnetTxt.getText().toString();
        String url = urlLocal + "carnet=" + carnet;
        String notaPromedioJSON = ControladorServicio.obtenerRespuestaPeticion( url, this);
        notaPromedioTxt.setText("Nota Promedio Externo: " +
                ControladorServicio.obtenerPromedioJSON(notaPromedioJSON,this));}


    public void consultarPromedioExterno(View v) {
        String carnet = carnetTxt.getText().toString();
        String url = urlHostingGratuito + "carnet=" + carnet;
        String notaPromedioJSON = ControladorServicio.obtenerRespuestaPeticion( url, this);
        notaPromedioTxt.setText("Nota Promedio Externo: " +
                ControladorServicio.obtenerPromedioJSON(notaPromedioJSON,this)); }
}
